require('dotenv').config();
let paraguayTimezone = () => {
  let timezonePy = new Date().toString().match(/([-\+][0-9]+)\s/)[1];
  return [timezonePy.slice(0,3), ':', timezonePy.slice(3)].join('')
}

module.exports = {
  "development": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PW,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOST,
    "dialect": process.env.DB_DIALECT,
    "timezone": paraguayTimezone()
  },
  "production": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PW,
    "database": process.env.DB_PW,
    "host": process.env.DB_HOST,
    "dialect": process.env.DB_DIALECT,
    "timezone": paraguayTimezone()
  },
};
