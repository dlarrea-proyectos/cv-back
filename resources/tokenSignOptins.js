let signOptionsToken = {
    issuer: process.env.JWT_ISSUER,
    subject: null,
    audience: process.env.JWT_AUDIENCE,
    expiresIn: process.env.JWT_TOKEN,
    algorithm: process.env.JWT_ALGORITHM
}

module.exports = {
    signOptionsToken
}