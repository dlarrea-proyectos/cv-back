const svgCaptcha = require('svg-captcha');
const models = require('../db/models');

let captchaGenerator = async(ip) => {
    svgCaptcha.loadFont('resources/Roboto-Regular.ttf');
    let captcha = svgCaptcha.create({
        size: 6,
        ignoreChars: '0oOlI',
        noise: 5,
        color: true,
        background: '#eff3f8',
        width: 200,
        height: 75,
        fontSize: 32
    });

    await models.Captcha.create({ip, value: captcha.text});
    return captcha;
}

let checkCaptcha = async(ip, value) => {
    return await models.Captcha.count({where:{ip, value}});
}



module.exports = {
    captchaGenerator,
    checkCaptcha
}