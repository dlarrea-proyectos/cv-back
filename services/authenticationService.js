const models = require('../db/models');
const fs = require('fs');
const ErrorApp = require('../utils/Error');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
const privateKey = fs.readFileSync('resources/private.key');
const jwt = require('jsonwebtoken');
const { signOptionsToken } = require('../resources/tokenSignOptins');
const { activarCuentaMail, cambiarClaveMail, activarCuentaMailImportar } = require('./mailService');
const { Sequelize, Op } = require('sequelize');

let login = async({email, clave}) => {

    let user = await models.User.findOne(
        {
            attributes: ['id','nombres', 'apellidos', 'email', 'RolId', 'activo', 'ultimoInicioSesion', 'password'], 
            where:{email},
            include:[
                {model:models.Rol, as:'rol'},
                {model:models.Persona, as:'persona', attributes:['cargo']},
            ]
        }
    );
    
    if(user == null) throw new ErrorApp('Error en la autenticación', 404);
    
    if(!user.activo) throw new ErrorApp('Error en la autenticación', 400);
    
    if(!bcrypt.compareSync(clave, user.password)) throw new ErrorApp('Error en la autenticación', 400);
    await models.User.update({ultimoInicioSesion: new Date()}, {where:{id:user.id}});

    signOptionsToken.subject = email;

    return {
        access_token: jwt.sign({
            id: user.id,
            email: user.email,
            nombres: user.nombres,
            apellidos: user.apellidos,
            ultimoInicioSesion: user.ultimoInicioSesion,
            rol: user.rol.nombre,
            cargo: user.persona.cargo
        }, privateKey, signOptionsToken)
    }
    
}

let register = async({documento, nombres, apellidos, email, clave, cargo, rol=null}) => {

    let emailExists = await models.User.count({where:{email}});
    if(emailExists !== 0) throw new ErrorApp('El email ya se encuentra en uso', 400);
    
    let documentoExists = await models.Persona.count({where:{documento}});
    if(documentoExists !== 0) throw new ErrorApp('El nro. de documento ya se encuentra en uso', 400);

    let encryptedPw = bcrypt.hashSync(clave, salt);
    let rolUsuario = rol == null ? await models.Rol.findOne({where:{nombre:'VISITANTE'}}) : await models.Rol.findOne({where:{id:rol}});
    let estado = await models.Estado.findOne({where:{nombre:'NUEVO'}});
    let {dataValues} = await models.User.create({email, password: encryptedPw, nombres, apellidos, RolId: rolUsuario.id, EstadoId: estado.id});
    
    await models.Persona.create({documento, nombres, apellidos, UserId: dataValues.id, cargo});

    activarCuentaMail(dataValues);
 
}

let registerImportar = async({documento, nombres, apellidos, email, celular, ciudad, direccion, indigena, rol, estado}) => {

    let emailExists = await models.User.count({where:{email}});
    if(emailExists !== 0) throw new ErrorApp('El email ya se encuentra en uso', 400);
    
    let documentoExists = await models.Persona.count({where:{documento}});
    if(documentoExists !== 0) throw new ErrorApp('El nro. de documento ya se encuentra en uso', 400);

    let clave = Buffer.from(new Date().getTime().toString()).toString('base64');
    let encryptedPw = bcrypt.hashSync(clave, salt);
    
    
    let {dataValues} = await models.User.create({email, password: encryptedPw, nombres, apellidos, RolId: rol, EstadoId: estado, activo:true});
    
    await models.Persona.create({documento, nombres, apellidos, UserId: dataValues.id, CiudadId: ciudad, celular, direccion, indigena});

    activarCuentaMailImportar(dataValues, clave);
 
}

let activarCuenta = async({key}) => {

    let email = Buffer.from(key, 'base64').toString('utf-8');

    let user = await models.User.findOne({where:{email}});
    if(user.activo) throw new ErrorApp('La cuenta ya se encuentra activada', 400);
    
    if(!fs.existsSync(`uploads/user-${user.id}`)) fs.mkdirSync(`uploads/user-${user.id}`);
    
    await models.User.update({activo:true}, {where:{email}});
}

let cambiarClaveSolicitud = async({email}) => {

    let user = await models.User.findOne(
        {
            attributes: ['id', 'email', 'nombres', 'apellidos'], 
            where:{email}
        }
    );
    
    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    await models.CambiarClave.destroy({where:{UserId:user.id}});
    await models.CambiarClave.create({UserId:user.id});

    cambiarClaveMail(user);

}

let cambiarClave = async({key, clave}) => {

    let email = Buffer.from(key, 'base64').toString('utf-8');

    let user = await models.User.findOne(
        {
            attributes: ['id', 'email', 'nombres', 'apellidos'], 
            where:{email}
        }
    );
    
    if(user == null) throw new ErrorApp('El usuario no existe', 404);
    
    let solicitud = await models.CambiarClave.findOne({where:{UserId: user.id, createdAt: {[Op.gt]: Sequelize.literal("NOW() - INTERVAL 24 HOUR")}}});
    if(solicitud === null) throw new ErrorApp('Enlace expirado', 400);

    let encryptedPw = bcrypt.hashSync(clave, salt);
    await models.User.update({password:encryptedPw}, {where:{email}});
    await models.CambiarClave.destroy({where:{UserId:user.id}});

}

module.exports = {
    login,
    register,
    activarCuenta,
    cambiarClaveSolicitud,
    cambiarClave,
    registerImportar
}
