const nodemailer = require('nodemailer');
const ErrorApp = require('../utils/Error');

let activarCuentaMail = async(user) => {

    let transporter = nodemailer.createTransport({
        host: "smtp-relay.sendinblue.com",
        port: 587,
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_USER_PW,
        },
    });
    
    let mailOptions = {
        from: `MAG <${process.env.EMAIL_FROM}>`,
        to: user.email,
        subject: `Activar cuenta`,
        html: `
        <html>
            <head>
                <title>Activar cuenta</title>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
            </head>
            <body>
                <div style="font-size:16px;font-weight:bold;display:block;min-height:20px;line-height:20px;text-transform:uppercase;">${user.nombres} ${user.apellidos}</div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"></div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"><a href="${process.env.FRONT_URL}/login?key=${Buffer.from(user.email, 'utf-8').toString('base64')}">Para activar su cuenta haga click en este enlace</a></div>
            </body>
        </html>
        `,
    };

    let result = await new Promise(resolve => {
        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                resolve(false);
            } else {
                resolve(true)
            }
        });
    });

    if(!result) throw new ErrorApp('Error al enviar correo de confirmación', 500);
}

let activarCuentaMailImportar = async(user, clave) => {

    let transporter = nodemailer.createTransport({
        host: "smtp-relay.sendinblue.com",
        port: 587,
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_USER_PW,
        },
    });
    
    let mailOptions = {
        from: `MAG <${process.env.EMAIL_FROM}>`,
        to: user.email,
        subject: `Creación de cuenta`,
        html: `
        <html>
            <head>
                <title>Activar cuenta</title>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
            </head>
            <body>
                <div style="font-size:16px;font-weight:bold;display:block;min-height:20px;line-height:20px;text-transform:uppercase;">${user.nombres} ${user.apellidos}</div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"></div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;">Se ha creado su cuenta en el Portal de Censos MAG</div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"><b>Usuario</b> ${user.email}</div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"><b>Clave</b> ${clave}</div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"></div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;">Para ingresar al portal, haga click <a href="${process.env.FRONT_URL}/">aquí</a></div>
            </body>
        </html>
        `,
    };

    let result = await new Promise(resolve => {
        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                resolve(false);
            } else {
                resolve(true)
            }
        });
    });

    if(!result) throw new ErrorApp('Error al enviar correo de confirmación', 500);
}

let cambiarClaveMail = async(user) => {

    let transporter = nodemailer.createTransport({
        host: "smtp-relay.sendinblue.com",
        port: 587,
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_USER_PW,
        },
    });
    
    let mailOptions = {
        from: `App CV <${process.env.EMAIL_FROM}>`,
        to: user.email,
        subject: `Cambiar clave`,
        html: `
        <html>
            <head>
                <title>Activar cuenta</title>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
            </head>
            <body>
                <div style="font-size:16px;font-weight:bold;display:block;min-height:20px;line-height:20px;text-transform:uppercase;">${user.nombres} ${user.apellidos}</div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"></div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"><a href="${process.env.FRONT_URL}/cambiar-clave?key=${Buffer.from(user.email, 'utf-8').toString('base64')}">Para cambiar su clave haga click en este enlace</a></div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;"></div>
                <div style="font-size:14px;display:block;min-height:20px;line-height:20px;">Este enlace tiene una validez de 24hs.</div>
            </body>
        </html>
        `,
    };

    let result = await new Promise(resolve => {
        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                resolve(false);
            } else {
                resolve(true)
            }
        });
    });

    if(!result) throw new ErrorApp('Error al enviar correo de cambio de clave', 500);
}

module.exports = {
    activarCuentaMail,
    cambiarClaveMail,
    activarCuentaMailImportar
}