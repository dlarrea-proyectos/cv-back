const excelJS = require("exceljs");
const models = require('../db/models');
const readXlsxFile = require("read-excel-file/node");
const path = require('path');
const fs = require('fs');
const { validateEmail } = require('../utils/Helpers');
const { registerImportar } = require('./authenticationService');
const ErrorApp = require('../utils/Error');

let exportUsers = async() => {

    let usuarios = await models.User.findAll(
        {
            where: {activo:true},
            include:[
                {model:models.Persona, as:'persona', include:[{model:models.Ciudad, as:'ciudad', attributes:['nombre', 'departamento']}]}, 
                {model:models.Estado, as:'estado', attributes: ['nombre']},
                {model:models.Evaluacion, as:'evaluacion', attributes: ['formulario', 'puntaje']}
            ],
            attributes: ['nombres', 'apellidos', 'email', 'activo'], 
        }
    );
    const workbook = new excelJS.Workbook();
    const worksheet = workbook.addWorksheet("Usuarios"); 

    worksheet.columns = [
        { header: 'Nro. documento' , width: 20},
        { header: 'Nombres' , width: 20},
        { header: 'Apellidos' , width: 20},
        { header: 'Correo' , width: 20},
        { header: 'Celular' , width: 20},
        { header: 'Ciudad' , width: 20},
        { header: 'Dirección' , width: 20},
        { header: 'Comunidad Indígena' , width: 20},
        { header: 'Estado' , width: 20},
        { header: '¿Tiene 18 años de edad cumplidos?' , width: 20},
        { header: '¿Actualmente es Contratado/Funcionario de alguna Entidad Pública? O Consejal Municipal?' , width: 20},
        { header: '¿Posee Título Universitario,de tecnicaturas, o   de las carreras  afines  al perfil requerido en este llamado  validado por el MEC?' , width: 20},
        { header: '¿Posee certificado de  bachiller terminado?, o   constancia de su Institución Educativa en donde está cursando, o de de cursar alguna tecnicatura agropecuaria?' , width: 20},
        { header: '¿Cuántos años de experiencia comprobable posee en las siguientes áreas: Ingenieria Agronómica, Ciencias Veterinarias, Zootecnia, Economía, Administración Agraria, Estadística, Técnicos de Mando Medio, Docente u otras profesiones relacionadas?' , width: 20},
        { header: '¿Está cursando el 2do o 3er año de la media o de alguna tecnicatura agropecuaria?' , width: 20},
        { header: '¿Cuenta con experiencia realizando algún trabajo en el rubro Agropecuario?' , width: 20},
        { header: '¿Posee experiencia en trabajos de Censo o Encuestas?' , width: 20},
        { header: '¿Posee experiencia en proyecto o programa rural?' , width: 20},
        { header: '¿Ha liderado equipos de trabajo?' , width: 20},
        { header: '¿Dispone de tiempo completo?' , width: 20},
        { header: 'Seleccione la ciudad en donde fija residencia permanente' , width: 20},
        { header: '¿Posee excelente manejo de paquete office (excel, word)? Y manejo de dispositivos inteligentes tales como: Tablets o Teléfonos.' , width: 20},
        { header: '¿Habla Guaraní?' , width: 20},
        { header: '¿Posee movilidad propia?' , width: 20},
        { header: '¿A través de que medio te enteraste de la búsqueda?' , width: 20},
        { header: '¿Pertenece a alguna comunidad indígena?' , width: 20},
        { header: 'Puestos' , width: 20},
        { header: 'Puntaje' , width: 20},
    ];

    usuarios.forEach(el => {
        
        let form = null; let puntaje = null;let ciudad = null;
        if(el.evaluacion) {
            let { dataValues } = el.evaluacion;
            if(dataValues) {
                form = JSON.parse(dataValues.formulario);
                puntaje = dataValues.puntaje;
            };
        }
        
        if(el.persona.ciudad){
            let { dataValues } = el.persona.ciudad;
            if(dataValues) {
                ciudad = `${dataValues.nombre} - ${dataValues.departamento}`;
            };
        }

        let row = [
            el.persona.documento,
            el.nombres,
            el.apellidos,
            el.email,
            el.persona.celular,
            ciudad,
            el.persona.direccion,
            el.persona.indigena,
            el.estado.nombre,
            form ? (form.p1 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p2 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p3 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p4 == 1 ? 'SI' : 'NO'):'NR',
            form ? form.p5 :'NR',
            form ? (form.p6 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p7 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p8 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p9 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p10 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p11 == 1 ? 'SI' : 'NO'):'NR',
            form ? form.p12:'NR',
            form ? (form.p13 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p14 == 1 ? 'SI' : 'NO'):'NR',
            form ? (form.p15 == 1 ? 'SI' : 'NO'):'NR',
            form ? form.p16:'NR',
            form ? (form.p17 == 1 ? 'SI' : 'NO'):'NR',
            el.persona.cargo,
            puntaje
        ];
        worksheet.addRow(row);
    });

    worksheet.getRow(1).eachCell((cell) => {
        cell.font = { bold: true };
    });

    try {

        let fileName = `${new Date().getTime()}.xlsx`;
        await workbook.xlsx.writeFile(`files/${fileName}`);
        return fileName;

    } catch(e){
        return null;
    }
}

let importUsers = async(file) => {

    let rol = await models.Rol.findOne({where:{nombre:'VISITANTE'}});
    let estado = await models.Estado.findOne({where:{nombre:'NUEVO'}});
    let usuarios = await new Promise(resolve => {
        readXlsxFile(path.join(global.__basedir, 'files', file.filename)).then((rows)=> {
            let usuarios = [];
            rows.shift();
            for(let row of rows){
                let usuario = {
                    documento: Number(row[0]),
                    nombres: String(row[1]),
                    apellidos: String(row[2]),
                    email: String(row[3]),
                    celular: String(row[4]),
                    ciudad: String(row[5]),
                    direccion: String(row[6]),
                    indigena: String(row[7]),
                    rol: rol.id,
                    estado: estado.id
                }
                usuarios.push(usuario);
            }
            resolve(usuarios);
        });
    });
    
    let log = [];
    for(let usuario of usuarios){
        try {

            if(isNaN(usuario.documento)) throw new ErrorApp('El documento debe ser numérico', 400);
            if (!/595[6-9]\d[\s\D]*\d{3}[\s\D]*\d{3}/.test(usuario.celular)) throw new ErrorApp('Formato de celular incorrecto', 400);
            if(!validateEmail(usuario.email)) throw new ErrorApp('Email inválido', 400);
            let ciudad = await models.Ciudad.findOne({where:{nombre:usuario.ciudad}});
            if(ciudad == null) throw new ErrorApp('La ciudad no existe', 400);
            usuario.ciudad = ciudad.id;
            await registerImportar(usuario);
            log.push(`[INFO:Registro importado] ${JSON.stringify(usuario)}`);

        } catch(e){
            log.push(`[ERROR:${e.message}] ${JSON.stringify(usuario)}`);
        }
    }

    let logFileName = `log_${new Date().getTime()}.txt`;
    fs.writeFileSync(path.join(global.__basedir, 'files', logFileName), log.join('\n'));
    return log;
}

let descargarUserTemplate = async() => {

    let ciudades = await models.Ciudad.findAll({order:['nombre']});
    const workbook = new excelJS.Workbook();
    const worksheetTemplate = workbook.addWorksheet("Template");
    
    worksheetTemplate.columns = [
        { header: 'Nro. documento' , width: 20},
        { header: 'Nombres' , width: 20},
        { header: 'Apellidos' , width: 20},
        { header: 'Email' , width: 20},
        { header: 'Celular (5959XXXXXXXX)' , width: 20},
        { header: 'Ciudad (Utilize la hoja Ciudades)' , width: 20},
        { header: 'Dirección' , width: 20},
        { header: 'Comunidad Indígena' , width: 20},
    ];

    worksheetTemplate.getRow(1).eachCell((cell) => {
        cell.font = { bold: true };
    });

    const worksheetCiudades = workbook.addWorksheet("Ciudades");

    worksheetCiudades.columns = [
        { header: 'Ciudad' , width: 20},
        { header: 'Departamento' , width: 20},
    ];

    worksheetCiudades.getRow(1).eachCell((cell) => {
        cell.font = { bold: true };
    });

    ciudades.forEach(el => {
        let row = [el.nombre, el.departamento];
        worksheetCiudades.addRow(row);
    });

    try {

        let fileName = `template-${new Date().getTime()}.xlsx`;
        await workbook.xlsx.writeFile(`files/${fileName}`);
        return fileName;

    } catch(e){
        return null;
    }
}

module.exports = { exportUsers, importUsers, descargarUserTemplate }