const models = require('../db/models');

let view = async() => {
    return await models.Ciudad.findAll({
        order: [
            ['nombre']
        ],
        attributes: ['id','nombre', 'departamento'],
    });
} 

let create = async({nombre, departamento}) => {
    nombre = String(nombre).toUpperCase();
    departamento = String(departamento).toUpperCase();
    return await models.Ciudad.create({nombre, departamento});
}

let update = async(id, {nombre, departamento}) => {
    nombre = String(nombre).toUpperCase();
    departamento = String(departamento).toUpperCase();
    return await models.Ciudad.update({nombre, departamento}, {where:{id}});
}

let destroy = async(id) => {
    return await models.Ciudad.destroy({where:{id}});
}


module.exports = {
    view,
    create,
    update,
    destroy
}