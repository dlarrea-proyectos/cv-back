const models = require('../db/models');
const ErrorApp = require('../utils/Error');

let view = async() => {
    return await models.Estado.findAll({attributes: ['id','nombre']});
} 

let create = async({nombre}) => {
    nombre = String(nombre).toUpperCase();
    return await models.Estado.create({nombre});
}

let update = async(id, {nombre}) => {
    nombre = String(nombre).toUpperCase();
    return await models.Estado.update({nombre}, {where:{id}});
}

let destroy = async(id) => {
    let estado = await models.Estado.findOne({where:{id}});
    console.log(estado);
    if(estado.nombre == 'NUEVO' || estado.nombre == 'NA' || estado.nombre == 'ENCUESTADO') throw new ErrorApp(`El estado ${estado.nombre} no puede ser eliminado`, 400);
    return await models.Estado.destroy({where:{id}});
}


module.exports = {
    view,
    create,
    update,
    destroy
}