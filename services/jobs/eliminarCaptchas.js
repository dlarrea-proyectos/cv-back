const models = require('../../db/models');
const { Sequelize, Op } = require('sequelize');

let eliminarCaptchas = async() => {
    console.log('Cron eliminarCaptchas running');
    await models.Captcha.destroy({where:{createdAt: {[Op.lt]: Sequelize.literal("NOW() - INTERVAL 2 MINUTE")}}});
}

module.exports = {
    eliminarCaptchas
}