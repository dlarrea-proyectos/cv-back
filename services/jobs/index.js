const cron = require('node-cron');
const { eliminarCaptchas } = require('./eliminarCaptchas');

let cronJobs = () => {

    cron.schedule('*/2 * * * *', async() => {
        await eliminarCaptchas();
    });
    
}

module.exports = cronJobs;