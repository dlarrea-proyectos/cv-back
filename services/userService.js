const models = require('../db/models');
const ErrorApp = require('../utils/Error');
const dayjs = require('dayjs');
const path = require('path');
const fs = require('fs');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
const AdmZip = require('adm-zip');

let userData = async(id) => {

    let user = await models.User.findOne(
        {
            attributes: ['id','nombres', 'apellidos', 'email'], 
            where:{id},
            include:[{model:models.Persona, as:'persona'}]
        }
    );

    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    return user;
}

let userDataAdmin = async(id) => {

    let user = await models.User.findOne(
        {
            attributes: ['id','nombres', 'apellidos', 'email', 'EstadoId', 'RolId'], 
            where:{id},
            include:[{model:models.Rol, as:'rol', attributes: ['nombre']},{model:models.Persona, as:'persona'},{model:models.Log, as:'logs', include:[{model: models.User, as: 'admin'}]}]
        }
    );

    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    return user;
}

let updateUserData = async({nombres, apellidos, sexo, estadoCivil, fechaNacimiento, celular, direccion, indigena}, id) => {

    let user = await models.User.findOne(
        {
            attributes: ['id'], 
            where:{id}
        }
    );

    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    if(!dayjs(fechaNacimiento).isValid()) throw new ErrorApp('Fecha de nacimiento inválida', 400);

    return await models.Persona.update({nombres, apellidos, sexo, estadoCivil, fechaNacimiento, celular, direccion, indigena}, {where: {UserId:user.id}});

}

let uploadUserFiles = async(files, id, tipo) => {

    let user = await models.User.findOne(
        {
            attributes: ['id'], 
            where:{id}
        }
    );
    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    // let fileCount = await models.Archivo.count({where:{UserId:user.id}});
    // if(fileCount > process.env.FILE_LIMIT - 1) throw new ErrorApp(`Solo puede subir ${process.env.FILE_LIMIT} archivos`, 400);
    
    
    for(let file of files){
        let { filename, mimetype } = file;
        if(tipo == 'Foto de perfil'){
            let foto = await models.Archivo.findOne({where:{UserId:id, tipo: 'Foto de perfil'}});
            if(foto != null){
                try {
                    fs.unlinkSync(path.join(global.__basedir, 'uploads', `user-${id}`, foto.file));
                } catch(e){
                    console.log(e);
                }
                await models.Archivo.destroy({where:{UserId:id, tipo: 'Foto de perfil'}});
            }
            if(!['image/jpeg', 'image/png'].includes(mimetype)) throw new ErrorApp('El archivo debe ser una imagen', 400);
        }
        await models.Archivo.create({UserId:user.id, file: filename, mime: mimetype, tipo});
    }
}

let getUserFiles = async(id) => {

    let user = await models.User.findOne(
        {
            attributes: ['id'], 
            where:{id}
        }
    );
    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    return await models.Archivo.findAll({where:{UserId:user.id}});
}

let getUserFoto = async(id) => {

    let user = await models.User.findOne(
        {
            attributes: ['id'], 
            where:{id}
        }
    );
    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    return await models.Archivo.findOne({where:{UserId:user.id, tipo:'Foto de perfil'}});
}

let fileExists = (file, folder) => {
    return fs.existsSync(path.join(global.__basedir, 'uploads', folder, file)); 
}

let downloadZip = (user) => {
    let folder =  `user-${user}`;
    if(fs.existsSync(path.join(global.__basedir, 'uploads', folder))){
        const zip = new AdmZip();
        let uploadDir = fs.readdirSync(path.join(global.__basedir, 'uploads', folder)); 
        for(let f of uploadDir){
            zip.addLocalFile(path.join(global.__basedir, 'uploads', folder, f));
        }
        return zip.toBuffer();
    }
    throw new ErrorApp('El usuario no posee archivos', 404);
}

let destroyFile = async(file, id) => {
    if(fileExists(file, `user-${id}`)){
        try {
            fs.unlinkSync(path.join(global.__basedir, 'uploads', `user-${id}`, file));
        } catch(e){
            console.log(e);
        }
    }
    await models.Archivo.destroy({where:{file}});
}

let resetPass = async({clave}, id) => {
    let encryptedPw = bcrypt.hashSync(clave, salt);
    await models.User.update({password:encryptedPw}, {where:{id}});
}

let getAll = async() => {
    
    let total = await models.User.count({where: {activo:true}});
    let usuarios = await models.User.findAll({
        include:[
            {model:models.Persona, as:'persona', attributes: ['documento', 'celular', 'nombres', 'apellidos']}, 
            {model:models.Estado, as:'estado', attributes: ['nombre']}
        ],
        attributes: ['id', 'nombres', 'apellidos', 'email', 'activo'],
        order: ['nombres'] 
    });
    return {total, usuarios};

}

let evaluacion = async(respuestas, user) => {
    
    let count = await models.Evaluacion.count({where:{UserId:user}});
    if(count !== 0) throw new ErrorApp('El usuario ya realizó la encuesta', 400);

    let puntaje = 0;
    
    puntaje += respuestas.p3;
    puntaje += respuestas.p4;

    switch(respuestas.p5){
        case '0 a 1 años':
            puntaje += 2;
            break;
        case '2 a 3 años':
            puntaje += 3;
            break;
        case '4 a 5 años':
            puntaje += 4;
            break;
        case '6 a 7 años':
            puntaje += 5;
            break;
        case '8 a 9 años':
            puntaje += 6;
            break;
        case '10 a 11 años':
            puntaje += 7;
            break;
        case '12 a 13 años':
            puntaje += 8;
            break;
        case '14 a 15 años':
            puntaje += 9;
            break;
        case 'Más de 15 años':
            puntaje += 10;
            break;
        default:
            puntaje += 0;
            break;
    }

    puntaje += respuestas.p6;
    puntaje += respuestas.p7;
    puntaje += respuestas.p8;
    puntaje += respuestas.p9;
    puntaje += respuestas.p10;
    puntaje += respuestas.p11;
    puntaje += respuestas.p13;
    puntaje += respuestas.p14;
    puntaje += respuestas.p15;

    puntaje = respuestas.p1 * puntaje;
    if(respuestas.p2 == 1){
        puntaje = 0;
    }

    if(puntaje == 0) {
        let estado = await models.Estado.findOne({where:{nombre:'NA'}});
        await models.User.update({EstadoId:estado.id}, {where:{id:user}});
    };

    let estado = await models.Estado.findOne({where:{nombre:'ENCUESTADO'}});
    await models.User.update({EstadoId:estado.id}, {where:{id:user}});
    await models.Persona.update({CiudadId:respuestas.p18}, {where:{id:user}});

    return await models.Evaluacion.create({formulario: JSON.stringify(respuestas), puntaje, UserId:user});

}

let getUserEvaluacion = async(user) => {

    return await models.Evaluacion.count({where:{UserId:user}});

}

let updateUserDataAdmin = async({nombres, apellidos, sexo, estadoCivil, fechaNacimiento, celular, CiudadId, direccion, indigena, id, EstadoId, RolId}, userAdmin) => {

    let user = await models.User.findOne(
        {
            attributes: ['id', 'EstadoId'], 
            where:{id}
        }
    );

    if(user == null) throw new ErrorApp('El usuario no existe', 404);

    if(!dayjs(fechaNacimiento).isValid()) throw new ErrorApp('Fecha de nacimiento inválida', 400);

    await historial({nombres, apellidos, sexo, estadoCivil, fechaNacimiento, celular, CiudadId, direccion, indigena, id, EstadoId}, userAdmin);

    if(user.EstadoId !== EstadoId) await models.User.update({EstadoId}, {where:{id}});

    await models.User.update({nombres, apellidos, RolId}, {where:{id}});
    return await models.Persona.update({nombres, apellidos, sexo, estadoCivil, fechaNacimiento, celular, CiudadId, direccion, indigena}, {where: {UserId:user.id}});
 
}

let historial = async(data, userAdmin) => {
    let log = [];
    let user = await models.User.findOne(
        {
            where:{id: data.id},
            include: [{model: models.Persona, as: 'persona'}]
        }
    );
    if(user != null && user.persona){
        
        if(user.persona.nombres !== data.nombres) log.push(`Nombres ha cambiado de ${user.persona.nombres} a ${data.nombres}`);
        if(user.persona.apellidos !== data.apellidos) log.push(`Apellidos ha cambiado de ${user.persona.apellidos} a ${data.apellidos}`);
        if(user.persona.sexo !== data.sexo) log.push(`Sexo ha cambiado de ${user.persona.sexo} a ${data.sexo}`);
        if(user.persona.estadoCivil !== data.estadoCivil) log.push(`Estado civil ha cambiado de ${user.persona.estadoCivil} a ${data.estadoCivil}`);
        if(user.persona.fechaNacimiento !== data.fechaNacimiento) log.push(`Fec. Nac. ha cambiado de ${user.persona.fechaNacimiento} a ${data.fechaNacimiento}`);
        if(user.persona.celular !== data.celular) log.push(`Nro. celular ha cambiado de ${user.persona.celular} a ${data.celular}`);
        if(user.persona.direccion !== data.direccion) log.push(`Dirección ha cambiado de ${user.persona.direccion} a ${data.direccion}`);
        if(user.persona.indigena !== data.indigena) log.push(`Comunidad indígena ha cambiado de ${user.persona.indigena} a ${data.indigena}`);
        if(user.persona.CiudadId !== data.CiudadId) {
            let cn = await models.Ciudad.findOne({where:{id:data.CiudadId}});
            let cv = await models.Ciudad.findOne({where:{id:user.persona.CiudadId}});
            log.push(`Ciudad ha cambiado de ${cv ? cv.nombre : 'VACIO'} a ${cn ? cn.nombre : 'VACIO'}`);
        }
        if(user.EstadoId !== data.EstadoId) {
            let en = await models.Estado.findOne({where:{id:data.EstadoId}});
            let ev = await models.Estado.findOne({where:{id:user.EstadoId}});
            log.push(`Estado ha cambiado de ${ev ? ev.nombre : 'VACIO'} a ${en ? en.nombre : 'VACIO'}`);
        }

        if(log.length > 0) await models.Log.create({comentario: log.join(';'), UserId: data.id, AdminId: userAdmin});
    }
}

module.exports = {
    userData,
    updateUserData,
    uploadUserFiles,
    getUserFiles,
    fileExists,
    destroyFile,
    resetPass,
    getAll,
    evaluacion,
    getUserEvaluacion,
    updateUserDataAdmin,
    downloadZip,
    userDataAdmin,
    getUserFoto
}