const express = require('express');
const router = express.Router();
const { body } = require('express-validator');
const authenticationController = require('../controllers/authenticationController');

router.post(
    '/login', 
    body('email').not().isEmpty().trim().withMessage('Parámetro email requerido'),
    body('clave').not().isEmpty().trim().withMessage('Parámetro clave requerido'),
    authenticationController.login
);

router.post(
    '/register', 
    body('documento').isNumeric().withMessage('Parámetro documento requerido'),
    body('nombres').not().isEmpty().trim().withMessage('Parámetro nombres requerido'),
    body('apellidos').not().isEmpty().trim().withMessage('Parámetro apellidos requerido'),
    body('email').isEmail().withMessage('Parámetro email requerido'),
    body('clave').not().isEmpty().trim().withMessage('Parámetro clave requerido'),
    body('cargo').not().isEmpty().trim().withMessage('Parámetro cargo requerido'),
    authenticationController.register
);

router.post(
    '/activate', 
    body('key').not().isEmpty().trim().withMessage('Parámetro key requerido'),
    authenticationController.activarCuenta
);

router.post(
    '/cambiar-clave-solicitud', 
    body('email').not().isEmpty().trim().withMessage('Parámetro email requerido'),
    authenticationController.cambiarClaveSolicitud
);

router.post(
    '/cambiar-clave', 
    body('key').not().isEmpty().trim().withMessage('Parámetro key requerido'),
    body('captcha').not().isEmpty().trim().withMessage('Parámetro captcha requerido'),
    body('clave').not().isEmpty().trim().withMessage('Parámetro clave requerido'),
    authenticationController.cambiarClave
);

module.exports = router;