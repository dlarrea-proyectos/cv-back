const app = require('express').Router();
const { authenticateToken } = require('../middlewares/authenticateToken');

app.use('/authentication', require('./authenticationRoute'));
app.use('/authentication', require('./captchaRoute'));
app.use('/user', authenticateToken, require('./userRoute'));
app.use('/admin', authenticateToken, require('./estadoRoute'));
app.use('/admin', authenticateToken, require('./rolRoute'));
app.use('/admin', authenticateToken, require('./ciudadRoute'));

module.exports = app;