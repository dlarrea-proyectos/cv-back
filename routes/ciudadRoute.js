const express = require('express');
const router = express.Router();
const { body, param } = require('express-validator');
const ciudadController = require('../controllers/ciudadController');

router.get('/ciudad', ciudadController.viewCiudad);
router.post(
    '/ciudad', 
    body('nombre').not().isEmpty().trim().withMessage('Parámetro nombre requerido'),
    body('departamento').not().isEmpty().trim().withMessage('Parámetro nombre requerido'),
    ciudadController.createCiudad
    );
    router.put(
        '/ciudad/:id', 
    body('nombre').not().isEmpty().trim().withMessage('Parámetro nombre requerido'),
    body('departamento').not().isEmpty().trim().withMessage('Parámetro nombre requerido'),
    param('id').isNumeric().withMessage('Parámetro id requerido'),
    ciudadController.updateCiudad
);
router.delete(
    '/ciudad/:id', 
    param('id').isNumeric().withMessage('Parámetro id requerido'),
    ciudadController.deleteCiudad
);

module.exports = router;