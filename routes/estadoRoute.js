const express = require('express');
const router = express.Router();
const { body, param } = require('express-validator');
const estadoController = require('../controllers/estadoController');

router.get('/estado', estadoController.viewEstado);
router.post(
    '/estado', 
    body('nombre').not().isEmpty().trim().withMessage('Parámetro nombre requerido'),
    estadoController.createEstado
);
router.put(
    '/estado/:id', 
    body('nombre').not().isEmpty().trim().withMessage('Parámetro nombre requerido'),
    param('id').isNumeric().withMessage('Parámetro id requerido'),
    estadoController.updateEstado
);
router.delete(
    '/estado/:id', 
    param('id').isNumeric().withMessage('Parámetro id requerido'),
    estadoController.deleteEstado
);

module.exports = router;