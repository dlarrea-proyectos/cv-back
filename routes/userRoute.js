const express = require('express');
const router = express.Router();
const { param, body, check } = require('express-validator');
const userController = require('../controllers/userController');
const { upload, importUpload } = require('../middlewares/multer');

const uploadStorage = upload();
const uploadStorageImportar = importUpload();

router.get(
    '/', 
    userController.getAll
);

router.get(
    '/template', 
    userController.descargarUserTemplate
);

router.get(
    '/:id/datos', 
    param('id').isNumeric().withMessage('Parámetro id requerido'),
    userController.getUserData
);

router.get(
    '/:id/datos-admin', 
    param('id').isNumeric().withMessage('Parámetro id requerido'),
    userController.getUserDataAdmin
);

router.get(
    '/evaluacion/count', 
    userController.getUserEvaluacion
);

router.post(
    '/evaluacion',
    check('p1').isIn([1,0]).withMessage('Parámetro p1 requerido'), 
    check('p2').isIn([1,0]).withMessage('Parámetro p2 requerido'), 
    check('p3').isIn([1,0]).withMessage('Parámetro p3 requerido'), 
    check('p4').isIn([1,0]).withMessage('Parámetro p4 requerido'), 
    check('p5').isIn([
        '0 a 1 años',
        '2 a 3 años',
        '4 a 5 años',
        '6 a 7 años',
        '8 a 9 años',
        '10 a 11 años',
        '12 a 13 años',
        '14 a 15 años',
        'Más de 15 años',
    ]).withMessage('Parámetro p5 requerido'), 
    check('p6').isIn([1,0]).withMessage('Parámetro p6 requerido'),
    check('p7').isIn([1,0]).withMessage('Parámetro p7 requerido'),
    check('p8').isIn([1,0]).withMessage('Parámetro p8 requerido'),
    check('p9').isIn([1,0]).withMessage('Parámetro p9 requerido'),
    check('p10').isIn([1,0]).withMessage('Parámetro p10 requerido'),
    check('p11').isIn([1,0]).withMessage('Parámetro p11 requerido'),
    check('p12').not().isEmpty().trim().withMessage('Parámetro p12 requerido'),
    check('p13').isIn([1,0]).withMessage('Parámetro p13 requerido'),
    check('p14').isIn([1,0]).withMessage('Parámetro p14 requerido'),
    check('p15').isIn([1,0]).withMessage('Parámetro p15 requerido'),
    check('p16').not().isEmpty().trim().withMessage('Parámetro p16 requerido'),
    check('p17').isIn([1,0]).withMessage('Parámetro p17 requerido'),
    userController.evaluacion
);

router.post(
    '/datos', 
    body('nombres').not().isEmpty().trim().withMessage('Parámetro email requerido'),
    body('apellidos').not().isEmpty().trim().withMessage('Parámetro email requerido'),
    body('sexo').not().isEmpty().trim().withMessage('Parámetro sexo requerido'),
    body('estadoCivil').not().isEmpty().trim().withMessage('Parámetro estadoCivil requerido'),
    body('fechaNacimiento').not().isEmpty().trim().withMessage('Parámetro fechaNacimiento requerido'),
    body('celular').isNumeric().withMessage('Parámetro celular requerido'),
    body('direccion').not().isEmpty().trim().withMessage('Parámetro dirección requerido'),
    userController.updateUserData
);

router.post(
    '/datos-admin', 
    body('id').isNumeric().withMessage('Parámetro id requerido'),
    body('nombres').not().isEmpty().trim().withMessage('Parámetro email requerido'),
    body('apellidos').not().isEmpty().trim().withMessage('Parámetro email requerido'),
    body('sexo').not().isEmpty().trim().withMessage('Parámetro sexo requerido'),
    body('estadoCivil').not().isEmpty().trim().withMessage('Parámetro estadoCivil requerido'),
    body('fechaNacimiento').not().isEmpty().trim().withMessage('Parámetro fechaNacimiento requerido'),
    body('celular').isNumeric().withMessage('Parámetro celular requerido'),
    body('CiudadId').isNumeric().withMessage('Parámetro ciudad requerido'),
    body('RolId').isNumeric().withMessage('Parámetro rol requerido'),
    body('direccion').not().isEmpty().trim().withMessage('Parámetro dirección requerido'),
    userController.updateUserDataAdmin
);

router.post(
    '/archivos', 
    uploadStorage.array('files', 1),
    userController.uploadUserFiles
);

router.post(
    '/importar', 
    uploadStorageImportar.single('template'),
    userController.importUsers
);

router.get(
    '/archivos', 
    userController.getUserFiles
);

router.get(
    '/foto', 
    userController.getUserFoto
);

router.get(
    '/archivos-admin/:id', 
    userController.exportUserFiles
);

router.get(
    '/archivos/:file', 
    userController.getFile
);

router.delete(
    '/archivos/:file', 
    userController.destroyFile
);

router.get(
    '/exportar', 
    userController.exportUsers
);

router.post(
    '/cambiar-clave', 
    body('captcha').not().isEmpty().trim().withMessage('Parámetro captcha requerido'),
    body('clave').not().isEmpty().trim().withMessage('Parámetro clave requerido'),
    userController.resetPass
);

module.exports = router;