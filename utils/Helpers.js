let extractToken = (headers) => {
    if (headers.authorization && headers.authorization.split(' ')[0] === 'Bearer') {
        return headers.authorization.split(' ')[1];
    }
    return null;
}

let getIp = (req) => {
    let ip = null;
    if(req.headers['x-forwarded-for'])
        ip = req.headers['x-forwarded-for'];
    else if(req.socket && req.socket.remoteAddress) 
        ip = req.socket.remoteAddress;
    return ip;
}

let validateEmail = (email) => {
    return String(email)
    .toLowerCase()
    .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

module.exports = { extractToken, getIp, validateEmail }