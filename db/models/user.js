'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Rol, {as:'rol', foreignKey: 'RolId'});
      this.belongsTo(models.Estado, {
        as: 'estado', 
        foreignKey: 'EstadoId'
      });
      this.hasOne(models.Persona, {
        as: 'persona',
        foreignKey: 'UserId'
      });
      this.hasMany(models.Archivo, {
        as: 'archivos',
        foreignKey: 'UserId'
      });
      this.hasOne(models.Evaluacion, {
        as: 'evaluacion',
        foreignKey: 'UserId'
      });
      this.hasMany(models.Log, {
        as: 'logs',
        foreignKey: 'UserId'
      });
      this.hasMany(models.Log, {
        as: 'actions',
        foreignKey: 'AdminId'
      });
    }
  }
  User.init({
    nombres: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    apellidos: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    activo: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    ultimoInicioSesion: {
      allowNull: true,
      type: DataTypes.DATE
    },
    RolId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: 'Rol', key: 'id' }
    },
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'User'
  });
  return User;
};