'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Estado extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.User, {
        as: 'users',
        foreignKey: 'EstadoId'
      });
    }
  }
  Estado.init({
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true
    },
  }, {
    sequelize,
    modelName: 'Estado',
    tableName: 'Estado'
  });
  return Estado;
};