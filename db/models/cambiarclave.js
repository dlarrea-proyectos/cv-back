'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CambiarClave extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  CambiarClave.init({
    UserId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: 'User', key: 'id' },
      unique: true
    },
  }, {
    sequelize,
    modelName: 'CambiarClave',
    tableName: 'CambiarClave'
  });
  return CambiarClave;
};