'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Persona extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User);
      this.belongsTo(models.Ciudad, {as: 'ciudad', foreignKey: 'CiudadId'});
    }
  }
  Persona.init({
    documento: {
      allowNull: false,
      unique: true,
      type: DataTypes.BIGINT 
    },
    nombres: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    apellidos: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    estadoCivil: {
      type: DataTypes.ENUM('SOLTERO/A', 'CASADO/A', 'DIVORCIADO/A', 'VIUDO/A'),
      defaultValue: 'SOLTERO/A'
    },
    fechaNacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    sexo: {
      type: DataTypes.ENUM('M', 'F'),
      defaultValue: 'M'
    },
    celular: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    UserId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: 'User', key: 'id' }
    },
    direccion: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    CiudadId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: { model: 'Ciudad', key: 'id' }
    },
    indigena: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    cargo: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Persona',
    tableName: 'Persona'
  });
  return Persona;
};