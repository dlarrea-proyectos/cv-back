'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rol extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.User, {
        as: 'users',
        foreignKey: 'RolId'
      });
    }
  }
  Rol.init({
    nombre: {
      type: DataTypes.STRING(50),
      unique: true,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Rol',
    tableName: 'Rol'
  });
  return Rol;
};