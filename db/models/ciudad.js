'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Ciudad extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Persona, {
        as: 'personas',
        foreignKey: 'CiudadId'
      });
    }
  }
  Ciudad.init({
    nombre: {
      type: DataTypes.STRING,
      allowNull: false
    },
    departamento: {
      type: DataTypes.ENUM('CAPITAL', 'CONCEPCIÓN', 'SAN PEDRO', 'CORDILLERA', 'GUAIRÁ', 'CAAGUAZÚ', 'CAAZAPÁ', 'ITAPÚA', 'MISIONES', 'PARAGUARÍ', 'ALTO PARANÁ', 'CENTRAL', 'ÑEEMBUCÚ', 'AMAMBAY', 'CANINDEYÚ', 'PRESIDENTE HAYES', 'BOQUERÓN', 'ALTO PARAGUAY'),
      allowNull: false,
      defaultValue: 'CAPITAL'
    },
  }, {
    sequelize,
    modelName: 'Ciudad',
    tableName: 'Ciudad'
  });
  return Ciudad;
};