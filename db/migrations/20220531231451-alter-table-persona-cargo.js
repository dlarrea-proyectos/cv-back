'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'Persona', 
      'cargo', 
      {
        type: Sequelize.STRING(100),
        allowNull: true
      }
    )
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'Persona', 
      'cargo', 
      {
        type: Sequelize.STRING(100),
        allowNull: true
      }
    )
  }
};
