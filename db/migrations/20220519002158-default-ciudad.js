'use strict';
const fs = require('fs');

module.exports = {
  async up (queryInterface, Sequelize) {

    let id = 1;
    let ciudades = [];
    let ciudadesJson = JSON.parse(fs.readFileSync('db/seeders/ciudades.json'));
    
    for(let c of ciudadesJson){
      ciudades.push({
        id: id,
        nombre: String(c.ciudad).toUpperCase(),
        departamento: String(c.departamento).toUpperCase(),
        createdAt: new Date(),
        updatedAt: new Date()
      });
      id ++;
    }
    
    try {
      queryInterface.bulkInsert('Ciudad', ciudades);
    } catch(e){
      throw new Error('Error inserting ciudades');
    }
  },

  async down (queryInterface, Sequelize) {}

};
