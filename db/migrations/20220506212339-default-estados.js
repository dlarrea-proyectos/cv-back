'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    const estados = [
      {
        id: 1,
        nombre: 'VISTO',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        nombre: 'A ENTREVISTAR',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        nombre: 'ARCHIVAR',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 4,
        nombre: 'SELECCIONADO',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 5,
        nombre: 'NUEVO',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 6,
        nombre: 'NA',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 7,
        nombre: 'ENCUESTADO',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ];
    
    try {
      queryInterface.bulkInsert('Estado', estados);
    } catch(e){
      throw new Error('Error inserting estados');
    }
  },

  async down (queryInterface, Sequelize) {}

};
