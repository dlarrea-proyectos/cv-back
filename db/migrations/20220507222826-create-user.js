'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('User', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombres: {
        type: Sequelize.STRING(150),
        allowNull: false
      },
      apellidos: {
        type: Sequelize.STRING(150),
        allowNull: false
      },
      email: {
        type: Sequelize.STRING(150),
        allowNull: false,
        unique: true
      },
      password: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      activo: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      ultimoInicioSesion: {
        allowNull: true,
        type: Sequelize.DATE
      },
      RolId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'Rol', key: 'id' }
      },
      EstadoId: {
        type: Sequelize.INTEGER,
        allowNull: null,
        references: { model: 'Estado', key: 'id' }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('User');
  }
};