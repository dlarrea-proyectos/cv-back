'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    const roles = [
      {
        id: 1,
        nombre: 'ADMIN',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        nombre: 'VISITANTE',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ];
    
    try {
      queryInterface.bulkInsert('Rol', roles);
    } catch(e){
      throw new Error('Error inserting roles');
    }
  },

  async down (queryInterface, Sequelize) {}

};
