'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Persona', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      documento: {
        allowNull: false,
        unique: true,
        type: Sequelize.BIGINT 
      },
      nombres: {
        type: Sequelize.STRING(150),
        allowNull: false
      },
      apellidos: {
        type: Sequelize.STRING(150),
        allowNull: false
      },
      estadoCivil: {
        type: Sequelize.ENUM('SOLTERO/A', 'CASADO/A', 'DIVORCIADO/A', 'VIUDO/A'),
        defaultValue: 'SOLTERO/A'
      },
      fechaNacimiento: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      sexo: {
        type: Sequelize.ENUM('M', 'F'),
        defaultValue: 'M'
      },
      celular: {
        type: Sequelize.STRING(100),
        allowNull: true
      },
      direccion: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      CiudadId: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: 'Ciudad', key: 'id' }
      },
      indigena: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      UserId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'User', key: 'id' }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Persona');
  }
};