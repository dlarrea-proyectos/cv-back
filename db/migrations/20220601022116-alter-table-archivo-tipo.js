'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'Archivo', 
      'tipo', 
      {
        type: Sequelize.STRING(100),
        allowNull: true
      }
    )
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'Archivo', 
      'tipo', 
      {
        type: Sequelize.STRING(100),
        allowNull: true
      }
    )
  }
};
