require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require("http");
const app = express();
const port = process.env.APP_PORT;
const server = http.createServer(app);
const routes = require('./routes');
const path = require('path');
const cronJobs = require('./services/jobs');
const { activarCuentaMail } = require('./services/mailService');
global.__basedir = __dirname;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

cronJobs();

app.use('/', express.static(path.join(__dirname, 'static')));
app.use('/api/v1', routes);

server.listen(port, () => {
    console.log(`Server running on port ${port}`);
});