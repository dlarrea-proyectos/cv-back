const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const userService = require('../services/userService');
const captchaService = require('../services/captchaService');
const excelService = require('../services/excelService');
const { validationResult } = require('express-validator');
const { getIp } = require('../utils/Helpers');
const path = require('path');
const { isAdmin } = require('../middlewares/isAdmin');

let getUserData = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        let user = await userService.userData(req.params.id);        
        return res.status(200).send(new Response('success', null, user, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let getUserDataAdmin = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        let user = await userService.userDataAdmin(req.params.id);        
        return res.status(200).send(new Response('success', null, user, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let updateUserData = async (req, res) => {
    try {
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        await userService.updateUserData(req.body, req.user);
        return res.status(200).send(new Response('success', 'Datos actualizados', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let updateUserDataAdmin = async (req, res) => {
    try {
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        await userService.updateUserDataAdmin(req.body, req.user);
        return res.status(200).send(new Response('success', 'Datos actualizados', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let uploadUserFiles = async (req, res) => {
    try {
        
        if(!req.headers.tipo) return res.status(400).send(new Response('error', 'Tipo de archivo requerido', null, 400, null));

        await userService.uploadUserFiles(req.files, req.user, req.headers.tipo)
        return res.status(200).send(new Response('success', 'Archivos guardados', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let getUserFiles = async (req, res) => {
    try {
        
        let files = await userService.getUserFiles(req.user)
        return res.status(200).send(new Response('success', null, files ? files : [], 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let getUserFoto = async (req, res) => {
    try {
        
        let foto = await userService.getUserFoto(req.user)
        return res.status(200).send(new Response('success', null, foto, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let getFile = async (req, res) => {
    try {

        let folder = `user-${req.user}`;
        if(!userService.fileExists(req.params.file, folder)) return res.status(400).send(new Response('error', 'El archivo no existe', null, 400, null));
        return res.sendFile(path.join(global.__basedir, 'uploads', folder, req.params.file));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let destroyFile = async (req, res) => {
    try {

        await userService.destroyFile(req.params.file, req.user)
        return res.status(200).send(new Response('success', 'Archivo eliminado', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let resetPass = async (req, res) => {
    try {

        let ip = getIp(req);
        if(ip == null) throw new ErrorApp('No se pudo determinar la ip de origen', 400);

        if(await captchaService.checkCaptcha(ip, req.body.captcha) === 0) throw new ErrorApp('Captcha inválida o expirada', 400);

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        await userService.resetPass(req.body, req.user)
        return res.status(200).send(new Response('success', 'Clave cambiada', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let getAll = async (req, res) => {
    try {

        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let {total, usuarios} = await userService.getAll();
        return res.status(200).send(new Response('success', null, usuarios, 200, {total}));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let exportUsers = async (req, res) => {
    try {

        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let filename = await excelService.exportUsers();
        if(filename == null) throw new ErrorApp('Error al generar archivo', 500);
        return res.download(path.join(global.__basedir, 'files', filename), filename);
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let exportUserFiles = async (req, res) => {
    try {

        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let zip = await userService.downloadZip(req.params.id);
        if(zip == null) throw new ErrorApp('Error al generar archivo', 500);
        res.set('Content-Type','application/octet-stream');
        res.set('Content-Disposition',`attachment; filename=${new Date().getTime()}.zip`);
        res.set('Content-Length', zip.length);
        res.send(zip);
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let evaluacion = async (req, res) => {
    try {

        await userService.evaluacion(req.body, req.user);
        return res.status(200).send(new Response('success', null, null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let getUserEvaluacion = async (req, res) => {
    try {

        let count = await userService.getUserEvaluacion(req.user);
        return res.status(200).send(new Response('success', null, count, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let importUsers = async (req, res) => {
    try {
        
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let log = await excelService.importUsers(req.file);
        return res.status(200).send(new Response('success', null, log, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let descargarUserTemplate = async (req, res) => {
    try {
        
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let filename = await excelService.descargarUserTemplate();
        if(filename == null) throw new ErrorApp('Error al generar archivo', 500);
        return res.download(path.join(global.__basedir, 'files', filename), filename);
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}


module.exports = {
    getUserData,
    updateUserData,
    uploadUserFiles,
    getUserFiles,
    getFile,
    destroyFile,
    resetPass,
    getAll,
    exportUsers,
    evaluacion,
    getUserEvaluacion,
    updateUserDataAdmin,
    exportUserFiles,
    importUsers,
    descargarUserTemplate,
    getUserDataAdmin,
    getUserFoto
}