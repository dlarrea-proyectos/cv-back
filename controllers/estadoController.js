const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const estadoService = require('../services/estadoService');
const { validationResult } = require('express-validator');
const { isAdmin } = require('../middlewares/isAdmin');

let viewEstado = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let rols = await estadoService.view();
        return res.status(200).send(new Response('success', null, rols, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let createEstado = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        await estadoService.create(req.body);
        return res.status(200).send(new Response('success', 'Registro creado', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let updateEstado = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        await estadoService.update(req.params.id, req.body);
        return res.status(200).send(new Response('success', 'Registro actualizado', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let deleteEstado = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        await estadoService.destroy(req.params.id);
        return res.status(200).send(new Response('success', 'Registro eliminado', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

module.exports = {
    viewEstado,
    createEstado,
    updateEstado,
    deleteEstado
}