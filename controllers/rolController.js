const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const rolService = require('../services/rolService');
const { isAdmin } = require('../middlewares/isAdmin');

let viewRol = async (req, res) => {
    try {
        
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        let roles = await rolService.view();
        return res.status(200).send(new Response('success', null, roles, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

module.exports = { viewRol }