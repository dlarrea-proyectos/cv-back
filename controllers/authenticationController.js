const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const authenticationService = require('../services/authenticationService');
const captchaService = require('../services/captchaService');
const { validationResult } = require('express-validator');
const { getIp } = require('../utils/Helpers')
const { isAdmin } = require('../middlewares/isAdmin');

let login = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        let result = await authenticationService.login(req.body);
        return res.status(200).send(new Response('success', 'Authentication successfully', result, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    
    }
}

let register = async (req, res) => {
    try {

        let ip = getIp(req);
        if(ip == null) throw new ErrorApp('No se pudo determinar la ip de origen', 400);

        if(await captchaService.checkCaptcha(ip, req.body.captcha) === 0) throw new ErrorApp('Captcha inválida o expirada', 400);

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        await authenticationService.register(req.body);
        return res.status(200).send(new Response('success', 'Usuario creado', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let activarCuenta = async (req, res) => {
    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        await authenticationService.activarCuenta(req.body);
        return res.status(200).send(new Response('success', 'Usuario activado', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let cambiarClaveSolicitud = async (req, res) => {
    try {

        let ip = getIp(req);
        if(ip == null) throw new ErrorApp('No se pudo determinar la ip de origen', 400);

        if(await captchaService.checkCaptcha(ip, req.body.captcha) === 0) throw new ErrorApp('Captcha inválida o expirada', 400);

        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        await authenticationService.cambiarClaveSolicitud(req.body);
        return res.status(200).send(new Response('success', 'Solicitud recibida', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let cambiarClave = async (req, res) => {
    try {

        let ip = getIp(req);
        if(ip == null) throw new ErrorApp('No se pudo determinar la ip de origen', 400);

        if(await captchaService.checkCaptcha(ip, req.body.captcha) === 0) throw new ErrorApp('Captcha inválida o expirada', 400);
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));

        await authenticationService.cambiarClave(req.body);
        return res.status(200).send(new Response('success', 'Contraseña cambiada', null, 200, null));
    
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

module.exports = {
    login,
    register,
    activarCuenta,
    cambiarClaveSolicitud,
    cambiarClave
}