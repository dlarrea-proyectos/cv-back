const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const captchaService = require('../services/captchaService');
const { getIp } = require('../utils/Helpers');

let getCaptcha = async (req, res) => {
    try {
        let ip = getIp(req);
        if(ip == null)throw new ErrorApp('No se pudo determinar la ip de origen');
        let captcha = await captchaService.captchaGenerator(ip);
        res.type('svg');
        return res.status(200).send(captcha.data);
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

module.exports = {
    getCaptcha
}