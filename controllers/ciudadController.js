const Response = require('../utils/Response');
const ErrorApp = require('../utils/Error');
const ciudadService = require('../services/ciudadService');
const { validationResult } = require('express-validator');
const { isAdmin } = require('../middlewares/isAdmin');

let viewCiudad = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        let ciudades = await ciudadService.view();
        return res.status(200).send(new Response('success', null, ciudades, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let createCiudad = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        await ciudadService.create(req.body);
        return res.status(200).send(new Response('success', 'Registro creado', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let updateCiudad = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        await ciudadService.update(req.params.id, req.body);
        return res.status(200).send(new Response('success', 'Registro actualizado', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

let deleteCiudad = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.status(400).send(new Response('error', errors.array(), null, 400, null));
        if(!isAdmin(req.user)) throw new ErrorApp('No tiene permisos para este recurso', 403);
        await ciudadService.destroy(req.params.id);
        return res.status(200).send(new Response('success', 'Registro eliminado', null, 200, null));
    } catch(e){
        if(e instanceof ErrorApp){
            return res.status(e.code).send(new Response('error', e.message, null, e.code, null));
        }
        return res.status(500).send(new Response('error', e.message || 'Server error', null,  500, null));
    }
}

module.exports = {
    viewCiudad,
    createCiudad,
    updateCiudad,
    deleteCiudad
}