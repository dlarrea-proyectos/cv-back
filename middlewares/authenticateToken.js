const jwt = require('jsonwebtoken');
const { extractToken } = require('../utils/Helpers');
const fs = require('fs');
const privateKey = fs.readFileSync('resources/private.key');
const Response = require('../utils/Response');

let authenticateToken = (req, res, next) => {
  
    let token = extractToken(req.headers);
    if (token == null) {
        if(req.query.key){
            token = req.query.key;
        } else {
            return res.status(401).send(new Response('error', 'Sesión expirada', null, 401, null));
        }
    } 
    let data = null;
    try {        
        data = jwt.verify(token, privateKey, { algorithms: ['RS256'] });
    } catch(err) {
        return res.status(401).send(new Response('error', 'Sesión expirada', null, 401, null));
    }
    req.user = data.id;
    next();

}

module.exports = { authenticateToken }