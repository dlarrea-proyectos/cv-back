const models = require('../db/models');
const ErrorApp = require('../utils/Error');
const { Op } = require("sequelize");

let isAdmin = async(data) => {
    let user = await models.User.findOne({
        where:{
            [Op.or] : [
                { id : data },
                { email: data }
            ]
        }, 
        include:{model:models.Rol, as:'rol'}
    });
    if(user == null) throw new ErrorApp('El usuario no existe', 404);
    return user.rol && user.rol.nombre == 'ADMIN';
}

module.exports = { isAdmin }