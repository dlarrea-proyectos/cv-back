const multer = require("multer");
const fs = require('fs');
const mime = require('mime-types');

let upload = () => {
    
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            if(!fs.existsSync(`uploads/user-${req.user}`)) fs.mkdirSync(`uploads/user-${req.user}`);
            cb(null, `uploads/user-${req.user}`);
        },
        filename: (req, file, cb) => {
            let ext = mime.extension(file.mimetype);
            cb(null, `file-${Date.now()}-${req.user}.${ext}`);
        }
    });

    return multer({ storage: storage, limits: { fileSize: 20000000 } });
}

let importUpload = () => {
    
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            if(!fs.existsSync(`files`)) fs.mkdirSync(`files`);
            cb(null, `files`);
        },
        filename: (req, file, cb) => {
            let ext = mime.extension(file.mimetype);
            cb(null, `importar-${Date.now()}.${ext}`);
        }
    });

    return multer({ storage: storage, limits: { fileSize: 20000000 } });
}

module.exports = {
    upload,
    importUpload
}